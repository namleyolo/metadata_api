package models

type ChannelIcons struct {
	id int `json:"id"`
	ChannelId int `json:"channel_id"`
	IconId int `json:"icon_id"`
	Version string `json:"version"`
}
