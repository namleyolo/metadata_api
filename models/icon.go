package models

type Icon struct {
	Id int `json:"id"`
	Name string `json:"name"`
	Uri string `json:"uri"`
}
