package models

import "time"

type ChannelCombine struct {
	Type    int       `json:"type"`
	Code    string    `json:"code"`
	Name    string    `json:"name"`
	Updated time.Time `json:"updated"`
	Icons   []string  `json:"icons"`
}

type SingleIcon struct {
	Icon string `json:"icon"`
}

type DeviceCombine struct {
	Type    int       `json:"type"`
	Code    string    `json:"code"`
	Name    string    `json:"name"`
	Updated time.Time `json:"updated"`
	Icons   []string  `json:"icons"`
}

type FinalResult struct {
	Version     string           `json:"version"`
	Channels    []ChannelCombine `json:"channels"`
	Devices     []DeviceCombine  `json:"devices"`
	RoomCombine []string         `json:"room_combine"`
}

type Homegates struct {
	Id       string    `json:"id"`
	State    int64     `json:"state"`
	IpPublic *string   `json:"ip_public"`
	Serial   string    `json:"serial"`
	Created  time.Time `json:"created"`
}

type HomegateDetail struct {
	ClientId string `json:"client_id"`
	IsOnline bool   `json:"is_online"`
	User     string `json:"user"`
	PeerHost string `json:"peer_host"`
}

type HomegateInfoFromBroker struct {
	Table []HomegateDetail `json:"table"`
	Type  string           `json:"type"`
}
