package models

import "time"

type Device struct {
	Id int `json:"id"`
	Type int `json:"type"`
	Name string `json:"name"`
	Code string `json:"code"`
	Updated time.Time `json:"updated"`
}
