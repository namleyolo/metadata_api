package models

type RoomIcons struct {
	Id int `json:"id"`
	IconId int `json:"icon_id"`
	Version string `json:"version"`
	Uri string `json:"uri"`
}
