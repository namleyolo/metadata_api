package models

type DeviceIcons struct {
	Id int `json:"id"`
	DeviceId int `json:"device_id"`
	IconId int `json:"icon_id"`
	Version string `json:"version"`
}
