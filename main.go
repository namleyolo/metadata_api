package main

import (
	"github.com/gin-gonic/contrib/sessions"
	//"crypto/tls"
	"github.com/gin-gonic/gin"
	"gitlab.com/vitconduck/metadata-api/controller"
	"log"
	"net/http"
	//"net/http"
)

const (
	userkey = "user"
)

func setupRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.StaticFS("/file", http.Dir("public"))

	r.Static("/upload", "./upload")
	r.Use(sessions.Sessions("admin_session", sessions.NewCookieStore([]byte("secret@Nam12345*@*#*SD@#*SDSD*@#***DƯ*#@*"))))
	r.LoadHTMLGlob("templates/*")

	r.POST("/login", controllers.Login)
	r.GET("/login", controllers.Login)

	r.GET("/update/dicomiot/homegate/:name", controllers.Download)
	client := r.Group("/api")
	{
		client.Static("/public", "./public")
		client.GET("/metadata/", controllers.GetAllMetadata)
		client.GET("/ip_public", controllers.Test)

		imageRoute := client.Group("/image")
		{
			imageRoute.GET("/:type/:icon-name", controllers.ShowImage)
		}

		client.GET("/download/:name", controllers.Download)
		authorized := client.Group("/admin")
		authorized.Use(AuthRequired)
		{
			authorized.GET("/file", controllers.File)
			authorized.DELETE("/file/:name", controllers.File)
			authorized.POST("/upload", controllers.UploadFile)
			authorized.GET("/devices", controllers.Devices)
			authorized.GET("/notification", controllers.Notification)
			//authorized.GET("/ws",controllers.Wshandler)
			//authorized.GET("/test",controllers.HandleConnections)
		}
	}

	return r
}

func AuthRequired(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get(userkey)

	if user == nil {
		c.Redirect(302, "/login")
		//c.JSON(http.StatusUnauthorized, gin.H{"error": "user needs to be signed in to access this service"})
		c.Abort()
		return
	}
	c.Next()
}

func main() {
	r := setupRouter()
	go r.Run(":8082")

	err := r.RunTLS(":8081", "./server.crt", "server.key")
	if err != nil {
		log.Fatal(err.Error())
	}
}
