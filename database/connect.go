package database

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
)

func MetadataDb() (db *gorm.DB) {
	db, err := gorm.Open("postgres", "user=metadataadmin dbname=metadata password=3cac6b3f6712624521618b18f849af464b84486bf01ffff20f99cc2d599bbf66 port=5433 host=171.244.49.128 sslmode=disable")
	//db, err := gorm.Open("postgres", "user=postgres dbname=metadata2 password=postgres port=5432 host=localhost sslmode=disable")

	if err != nil {
		panic(err.Error())
	}
	log.Print("connected to DB")

	return db
}

func Resourcedb() (db *gorm.DB) {
	//db, err := gorm.Open("postgres", "user=resourcedb dbname=resourcedb password=resourcedb port=5432 host=resourcedb sslmode=disable")
	db, err := gorm.Open("postgres", "user=postgres dbname=newest password=postgres port=5432 host=localhost sslmode=disable")

	if err != nil {
		panic(err.Error())
	}
	log.Print("connected to resource DB")

	return db
}
