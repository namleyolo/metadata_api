package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"gitlab.com/vitconduck/metadata-api/models"
	"io/ioutil"
	"log"
	"net/http"
)

var wsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func Wshandler(c *gin.Context) {
	conn, err := wsupgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		fmt.Println("Failed to set websocket upgrade: %+v", err)
		return
	}

	defer conn.Close()

	client := http.Client{}
	var hgInfoModel models.HomegateInfoFromBroker

	for {
		messageType, msg, err := conn.ReadMessage()
		fmt.Printf("msg>>>>>>>> %s", msg)
		x := fmt.Sprintf("%s", msg)

		req, err := http.NewRequest("GET", "http://0Jq77NCv0qU2ukgLiaepLkDKs2XEmf9E@cloud.dicomiot.com:9999/api/v1/session/show?--user="+x, nil)
		if err != nil {
			log.Print(err)
		}
		getResponseHgInfo, _ := client.Do(req)
		body, err := ioutil.ReadAll(getResponseHgInfo.Body)
		err = json.Unmarshal(body, &hgInfoModel)

		if err != nil {
			break
		}

		var publicIp string
		if len(hgInfoModel.Table) == 1 {
			publicIp = hgInfoModel.Table[0].PeerHost
		} else {
			publicIp = "nil"
		}
		err = conn.WriteMessage(messageType, []byte(publicIp))
		if err != nil {
			fmt.Print(err.Error())
		}

	}
}
