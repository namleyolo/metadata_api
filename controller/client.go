package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"gitlab.com/vitconduck/metadata-api/database"
	"gitlab.com/vitconduck/metadata-api/models"
	"io"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	//"gitlab.com/vitconduck/metadata-api/utils"
)

var start bool

const userkey = "user"

func GetAllMetadata(c *gin.Context) {
	db := database.MetadataDb()
	if db.Error != nil {
		panic(db.Error.Error())
	}

	header := c.GetHeader("version")
	log.Print("header", header)
	if len(header) == 0 {
		c.JSON(400, "Bad request")
	} else {
		var resultCn = make([]models.ChannelCombine, 0)
		//var resultDev  = make([]models.DeviceCombine, 0)

		getCn, err := db.Debug().Table("channels").
			Select("channels.type, channels.name, channels.code , channels.updated, icons.uri").
			Joins("Join channel_icons on channels.id = channel_icons.channel_id").
			Joins("Join icons on channel_icons.icon_id = icons.id").
			Where("channel_icons.version =? ", header).
			Rows()
		if err != nil {
			log.Println("err on query", err.Error())
			return
		}
		db.LogMode(true)
		defer getCn.Close()
		for getCn.Next() {
			var cn models.ChannelCombine
			var singleIcon string

			err = getCn.Scan(&cn.Type, &cn.Name, &cn.Code, &cn.Updated, &singleIcon)
			cn.Icons = append(cn.Icons, singleIcon)
			resultCn = append(resultCn, cn)
		}

		var combineResultChan = make([]models.ChannelCombine, 0)
		//var finalResultChan     = make([]models.ChannelCombine, 0)
		count := 0
		_ = count
		start = false

		for i := 0; i < len(resultCn); i++ {
			for j := 0; j < len(combineResultChan); j++ {
				if resultCn[i].Type == combineResultChan[j].Type {
					combineResultChan[j].Icons = append(combineResultChan[j].Icons, resultCn[i].Icons...)
					break
				}
			}
			combineResultChan = append(combineResultChan, resultCn[i])
		}

		encountered := map[int]bool{}
		var result []models.ChannelCombine

		for v := range combineResultChan {
			log.Print("val", combineResultChan[v].Icons)

			if encountered[combineResultChan[v].Type] == true {
			} else {
				log.Print("val New >> ", combineResultChan[v].Icons)
				encountered[combineResultChan[v].Type] = true
				result = append(result, combineResultChan[v])
			}
		}

		//getDv , err := db.Debug().Table("devices").
		//	Select("devices.type, devices.name, devices.code , devices.updated, icons.uri").
		//	Joins("Join device_icons on devices.id = device_icons.device_id").
		//	Joins("Join icons on device_icons.icon_id = icons.id").
		//	Rows()
		//if err != nil {log.Println("err on query",err.Error())}
		//db.LogMode(true)
		//defer  getDv.Close()
		//for getDv.Next(){
		//	var dv models.DeviceCombine
		//	var singleIcon string
		//
		//	err = getDv.Scan(&dv.Type,&dv.Name,&dv.Code, &dv.Updated, &singleIcon )
		//
		//	dv.Icons = append(dv.Icons, singleIcon)
		//	resultDev = append(resultDev, dv)
		//}

		//var uniqueDev []models.DeviceCombine
		//for _, v := range resultDev {
		//	for _, u := range uniqueDev {
		//		if v.Type == u.Type {
		//			v.Icons = append(v.Icons, u.Icons...)
		//			break
		//		}
		//	}
		//	uniqueDev = append(uniqueDev, v)
		//}

		//encounteredDev := map[int]bool{}
		//var finalResultDev  []models.DeviceCombine
		//
		//for i := len(uniqueDev)-1; i >= 0; i-- {
		//	//if encounteredDev[uniqueDev[i].Type] == true {
		//		// Do not add duplicate.
		//	//} else {
		//		encounteredDev[uniqueDev[i].Type] = true
		//		finalResultDev = append(finalResultDev, uniqueDev[i])
		//	//}
		//}

		finalRs := models.FinalResult{
			Version:  header,
			Channels: result,
			Devices:  nil,
			RoomCombine: []string{"image/room_icon/bedroom.svg", "image/room_icon/bathroom.svg",
				"image/room_icon/garage.svg", "image/room_icon/kitchen.svg", "image/room_icon/rooms.svg", "image/room_icon/toilet.svg"},
		}

		c.JSON(200, finalRs)
	}

	defer db.Close() // Hoãn lại việc close database connect cho đến khi hàm Read() thực hiệc xong
}

func ShowImage(c *gin.Context) {
	var imgFolder = "./upload"
	iconNameParams := c.Param("icon-name")
	iconTypeParams := c.Param("type")

	img, err := os.Open(imgFolder + "/" + iconTypeParams + "/" + iconNameParams)
	if err != nil {
		c.JSON(404, err.Error())
	}
	defer img.Close()

	getExtensionFile := iconNameParams[len(iconNameParams)-3:]

	var cType string
	switch getExtensionFile {
	case "gif":
		cType = "image/gif"
		break
	case "png":
		cType = "image/png"
		break
	case "jpeg":
	case "jpg":
		cType = "image/jpeg"
		break
	case "svg":
		cType = "image/svg+xml"
		break
	default:
		cType = "Sdasd"
	}

	c.Writer.Header().Set("Content-Type", cType)
	io.Copy(c.Writer, img)

}
func Download(c *gin.Context) {
	var imgFolder = "./file/"

	getHeader := c.GetHeader("x-header")
	log.Print(getHeader, c.ClientIP())
	log.Print(os.Getenv("KEYHEADER"))

	if getHeader != "@DicomBD3cxm@3csk22431af33mlskf*f2@dqwdqw2xcc@[-=-asd@" {
		c.JSON(403, gin.H{
			"message": "forbidden",
		})
	} else {
		fileNameParams := c.Param("name")

		img, err := os.Open(imgFolder + "/" + fileNameParams)
		if err != nil {
			c.JSON(404, err.Error())
		}
		defer img.Close()

		c.Writer.Header().Set("Content-Disposition", "Attachment")

		io.Copy(c.Writer, img)
	}

}

func Test(c *gin.Context) {
	c.JSON(200, c.ClientIP())
}

func Login(c *gin.Context) {
	switch c.Request.Method {
	case "GET":
		c.HTML(200, "login.html", nil)
	case "POST":
		session := sessions.Default(c)
		username := c.PostForm("username")
		password := c.PostForm("password")

		// Validate form input
		if strings.Trim(username, " ") == "" || strings.Trim(password, " ") == "" {
			c.HTML(http.StatusBadRequest, "login.html", gin.H{"error": "Parameters can't be empty"})
			return
		}

		//getPass := os.Getenv("PASSWORD")
		if username != "dicomiot2020" || password != "@DicomBD3cxm@3csk22431af33mlskf*f2@" {
			c.HTML(http.StatusUnauthorized, "login.html", gin.H{"error": "Authentication failed"})
			return
		}

		session.Set(userkey, username) // In real world usage you'd set this to the users ID
		session.Options(sessions.Options{MaxAge: 36000})
		if err := session.Save(); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to save session"})
			return
		}
		c.Redirect(http.StatusMovedPermanently, "/api/admin/file")
	}
	return
}
func UploadFile(c *gin.Context) {
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		c.Redirect(304, "/api/admin/upload")
		return
	}

	filename := header.Filename
	out, err := os.Create("file/" + filename)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	copyFile, err := io.Copy(out, file)
	log.Print(float64(copyFile))

	if err != nil {
		log.Fatal(err)
	}
	//filepath := "http://localhost:8081/file/" + filename
	c.Redirect(301, "/api/admin/file")
}

func File(c *gin.Context) {
	db := database.MetadataDb()
	if db.Error != nil {
		panic(db.Error.Error())
	}
	r, err := c.Cookie("device")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(r)
	}
	switch c.Request.Method {
	case "GET":
		var allFileName []string
		files, err := ioutil.ReadDir("./file")
		if err != nil {
			log.Print(err)
		}
		for _, file := range files {
			allFileName = append(allFileName, file.Name())
		}
		c.HTML(200, "file.html", gin.H{
			"fileName": allFileName,
		})
	case "DELETE":
		getNameFile := c.Request.URL.Query().Get("file_name")
		fmt.Println("getNameFile", getNameFile)
		err := os.Remove("./file/" + getNameFile)
		if err != nil {
			c.JSON(404, err)
			return
		}
		c.JSON(200, "Deleted "+getNameFile)
	}
}

func Devices(c *gin.Context) {
	db := database.Resourcedb()
	if db.Error != nil {
		panic(db.Error.Error())
	}
	defer db.Close()
	switch c.Request.Method {
	case "GET":
		var getPage int
		const limitRecords = 50
		getPageFromRequest := c.Request.URL.Query().Get("page")
		getHgIDFromRequest := c.Request.URL.Query().Get("hgid")

		if len(getPageFromRequest) == 0 {
			getPage = 1
		} else {
			convertPageToInt, err := strconv.Atoi(getPageFromRequest)
			if err != nil {
				c.JSON(400, gin.H{
					"error": "Bad request",
				})
				return
			}
			getPage = convertPageToInt
		}
		c.SetCookie("device", "test", 2000, "", "localhost", true, true)

		var homegates = make([]models.Homegates, 0)
		var wg sync.WaitGroup
		wg.Add(2)
		var totalHomegates int
		var hgResponse = make([]*models.Homegates, 0)

		go func() {
			defer wg.Done()
			query, err := db.Debug().Model(homegates).Where("id like ?", "%"+getHgIDFromRequest+"%").
				Or("serial like ?", "%"+getHgIDFromRequest+"%").
				Select("id, serial, created").Offset(limitRecords*getPage - limitRecords).Limit(limitRecords).Rows()
			if err != nil {
				c.JSON(200, gin.H{
					"error": err.Error(),
					"code":  "not found",
				})
				return
			}

			defer query.Close()

			var rp models.HomegateInfoFromBroker
			for query.Next() {
				client := http.Client{}
				hg := new(models.Homegates)
				err = query.Scan(&hg.Id, &hg.Serial, &hg.Created)
				if err != nil {
					log.Print(err)
				}
				req, err := http.NewRequest("GET", "http://0Jq77NCv0qU2ukgLiaepLkDKs2XEmf9E@cloud.dicomiot.com:9999/api/v1/session/show?--user="+hg.Id, nil)
				if err != nil {
					log.Print(err)
				}
				getResponseHgInfo, _ := client.Do(req)
				body, err := ioutil.ReadAll(getResponseHgInfo.Body)
				err = json.Unmarshal(body, &rp)
				if err != nil {
					c.JSON(500, gin.H{"error": "Oops! My fault"})
					return
				}
				if len(rp.Table) == 1 {
					hg.State = 1
					ipPublic := rp.Table[0].PeerHost
					hg.IpPublic = &ipPublic
				} else {
					hg.State = 0
					hg.IpPublic = nil
				}
				hg.Created = hg.Created.Local()
				hgResponse = append(hgResponse, hg)
			}
		}()
		go func() {
			defer wg.Done()
			db.Debug().Where("id like ?", "%"+getHgIDFromRequest+"%").Or("serial like ?", "%"+getHgIDFromRequest+"%").Model(homegates).Count(&totalHomegates)
		}()

		wg.Wait()
		var totalPage int
		totalPage = int(math.Ceil(float64(totalHomegates) / float64(limitRecords)))
		var pageLists []int
		for i := 0; i < totalPage; i++ {
			pageLists = append(pageLists, i+1)
		}

		c.HTML(200, "devices.html",
			gin.H{
				"homegates":   hgResponse,
				"total":       totalHomegates,
				"currentPage": getPage,
				"totalPage":   totalPage,
				"pageLists":   pageLists,
				"keySearch":   getHgIDFromRequest,
				"add": func(number int) int {
					return number + 1
				},
				"add2": func(number int) int {
					return number + 2
				},
			})

		db.Debug().LogMode(true)
		break
	default:
		c.Writer.WriteHeader(404)
		break
	}
}
func Notification(c *gin.Context) {
	switch c.Request.Method {
	case "GET":

		break
	default:
		c.Writer.Write([]byte("Err"))
	}
}
